package Mustafa.main ;
import Mustafa.shapes3d.* ;
import Mustafa.shapes.* ;


public class TEST3D {
	public static void main(String[] list) {
		Cylinder cyl1 = new Cylinder(5, 10) ;
		Cylinder cyl2 = new Cylinder(3, 7) ;
		Cylinder cyl3 = new Cylinder(6, 11) ;
		
		System.out.println("Cylinders") ;
		System.out.println() ;
		System.out.println(cyl1) ;
		System.out.println("Area = " + cyl1.area()) ;
		System.out.println("Volume = " + cyl1.volume()) ;
		System.out.println(cyl2) ;
		System.out.println("Area = " + cyl2.area()) ;
		System.out.println("Volume = " + cyl2.volume()) ;
		System.out.println(cyl3) ;
		System.out.println("Area = " + cyl3.area()) ;
		System.out.println("Volume = " + cyl3.volume()) ;
		System.out.println() ;
		
		Box box1 = new Box(3,4,5) ;
		Box box2 = new Box(12,10,8) ;
		Box box3 = new Box(11,11,11) ;
		
		System.out.println("Boxes") ;
		System.out.println() ;
		System.out.println(box1) ;
		System.out.println("Area = " + box1.area()) ;
		System.out.println("Volume = " + box1.volume()) ;
		System.out.println(box2) ;
		System.out.println("Area = " + box2.area()) ;
		System.out.println("Volume = " + box2.volume()) ;
		System.out.println(box3) ;
		System.out.println("Area = " + box3.area()) ;
		System.out.println("Volume = " + box3.volume()) ;
	}
	
	
}