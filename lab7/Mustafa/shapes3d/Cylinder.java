package Mustafa.shapes3d ;
import Mustafa.shapes.Circle ;

public class Cylinder extends Circle {
	
	private int height ;
	private String str;
	
	public Cylinder(int height, double radius) {
		super(radius) ;
		this.height = height ;
		
	}
	
	public double area() {
		return height * super.perimeter() + super.area() * 2 ;
	}
	
	public double volume () {
		return height * super.area() ;
	}
	
	public String toString() {
		str = "Height = " + height + ", Radius = " + super.radius ;
		return str ;
		
	}
	
}