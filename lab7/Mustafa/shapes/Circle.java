package Mustafa.shapes ;

public class Circle {

	public double radius ;
	
	public Circle(double r) {
		radius = r ;
	}
	
	
	public double area(){
		double area = Math.pow(radius, 2) * Math.PI ;
		return area ;
	}
	
	public double perimeter() {
		double perimeter = 2 * Math.PI * radius ;
		return perimeter ;
		
	}


}