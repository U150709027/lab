package Mustafa.shapes ;

public class Rectangle{
	public int width ;
	public int length ;
	public Rectangle(int a, int b) {
		width = a ;
		length = b ;
	}
	
	public int area(){
		return width * length ;
	}
	
	public int perimeter() {
		return (width + length) * 2 ;
	}
	
	
}