public class FindGreatest {
	public static void main(String[] args) {

		int greatest = Integer.parseInt(args[0]);
		
		for (int counter = 1; counter < args.length; counter++) {
			int next = Integer.parseInt(args[counter]);
			if (next > greatest)
				greatest = next ;
		}
		
		System.out.print("The greatest of these numbers is :" + greatest);
	}
}