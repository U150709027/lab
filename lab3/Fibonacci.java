public class Fibonacci {
	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		int counter = 1;
		int total ;
		if (number == 1){
			System.out.print("0, 1");
		}
		else {
			System.out.print("0, 1, 1");	
		}
		
		for (int total1 = 1; total1 + counter < number; ) {
			System.out.print(", ");
			total = total1 ;
			total1 = total1 + counter ;
			counter = total ;
			System.out.print(total1);
		}
	}
}