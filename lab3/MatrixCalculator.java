public class MatrixCalculator {
	
	public static void main(String[] args) {
		
		int[][] martrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};
		int[][] martrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};
		int[][] martrixT;
		martrixT = new int[3][3];
		
		System.out.print("{");
		
		for (int row = 0; row < 3; row++) {
			System.out.print("{");
			for (int column = 0; column < 3; column++) {
				martrixT[row][column] = martrixA[row][column] + martrixB[row][column];
				if (column == 2) {
					System.out.print(martrixT[row][column]);
				}
				else {
					System.out.print(martrixT[row][column] + ", ");
				}
			}
			if (row == 2) {
				System.out.print("}");
			}
			else {
				System.out.print("}, ");
			}
		}
		
		System.out.print("}");
	}
}