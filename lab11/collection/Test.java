package collection ;

import java.util.* ;

public class Test {
	
	public static void main(String[] args) {
		String values = "23 4 6 12 5 4 16 12 6" ;
		String[] arr = values.split(" ");
		Collection<String> col = new ArrayList<>() ;
		
		for(String str : arr) {
			col.add(str);
		}
		
		System.out.println("Normal set : " + col) ;
		
		col.add("4") ;
		System.out.println("Normal set : " + col) ;
		
		Set<String> set = new HashSet<>() ;
		
		for(String str : arr) {
			set.add(str);
		}
		
		System.out.println("Hash set : " + set) ;
		
		set.add("15") ;
		System.out.println("Hash set : " + set) ;
		
		Set<String> seti = new LinkedHashSet<>() ;
		
		for(String str : arr) {
			seti.add(str);
		}
		
		System.out.println("LinkedHash set : " + seti) ;
		
		seti.add("15") ;
		System.out.println("LinkedHash set : " + seti) ;
		
		Set<String> setii = new TreeSet<>() ;
		
		for(String str : arr) {
			setii.add(str);
		}
		
		System.out.println("Tree set : " + setii) ;
		
		setii.add("15") ;
		System.out.println("Tree set : " + setii) ;
		
		Set<String> setiii = new TreeSet<>(new NumberComparator()) ;
		
		for(String str : arr) {
			setiii.add(str);
		}
		
		System.out.println("Tree set com : " + setiii) ;
		
		setiii.add("15") ;
		System.out.println("Tree set com : " + setiii) ;
		
		
		Queue<String> que = new LinkedList<>() ;
		
		for(String str : arr) {
			que.add(str);
		}
		
		System.out.println("Queue : " + que) ;
		
		System.out.println("Removed : " + que.remove()) ;
		
		System.out.println("New Queue : " + que) ;
		
		
		Queue<String> quee = new PriorityQueue<>() ;
		
		for(String str : arr) {
			quee.add(str);
		}
		
		System.out.println("Priority Queue : " + quee) ;
		
		System.out.println("Removed : " + quee.remove()) ;
		
		System.out.println("New Priorty Queue : " + quee) ;
		
		
		Queue<String> queee = new PriorityQueue<>(new NumberComparator()) ;
		
		for(String str : arr) {
			queee.add(str);
		}
		
		System.out.println("Priority com Queue : " + queee) ;
		
		System.out.println("Removed : " + queee.remove()) ;
		
		System.out.println("New Priorty com Queue : " + queee) ;
		
		
		Queue<String> stack = Collections.asLifoQueue(new LinkedList<>()) ;
		
		for(String str : arr) {
			stack.add(str);
		}
		
		System.out.println("Priority com Queue Stack : " + stack) ;
		
		System.out.println("Removed Stack : " + stack.remove()) ;
		
		System.out.println("New Priorty com Queue Stack : " + stack) ;
		
	}
	
	
}