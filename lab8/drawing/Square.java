package drawing;

public class Square extends Shape {
	
	private int side;
	private Point topleft;
	
	public Square(int side, Point  topleft){
		this.side = side;
		this.topleft = topleft;
	}

	public void draw(){
		System.out.println("Drawing Square at " + topleft + " having side " + side);
	}
	
	public double area(){
		return side * side;
	}
	
	public void move(int xDistance,  int yDistance){
		topleft.move(xDistance, yDistance);
		
	}
}
