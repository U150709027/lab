// Questions 1,2,3,4,6,7

public class Midterm {
	
	public static void main(String[] args) {
		int a = 5;
		int b = a++;
		int c = ++a;
		int d = a++ + b-- + c++;
		System.out.println("a= " + a + ", b= " + b + ", c= " + c + ", d= " + d);
		
		
		System.out.println("Maximum number = " + max(5,6));
		System.out.println("F(6) = " + function(6));
		System.out.println("Hexa(175) = " + hexa(175));
		
		for (int i = 1; i <= 5; i++){
			for (int j = 1; j <= 5; j++)
				if ((i == j) || (i + j == 6)){
					System.out.print("X");
				}else{
					System.out.print("_");
				}
			System.out.println("");
		}
		
		
		int[] primeNumbers = new int[50] ;
		int index = 0 ;
		int number = 2 ;
		while (index < 50) {
			boolean isPrime = true ;
			
			
			for (int counter = 2; counter < number; counter++) {
				if (number % counter == 0) {
					isPrime = false ;
					break ;
				}
			}
			
			if (isPrime) {
				primeNumbers[index] = number ;
				index++ ;
			}
			number++ ;
		}
		
		for(int i=0; i< 50; i++){
			System.out.print(primeNumbers[i] + " ");
		}
		
	}
	
	
	public static int function(int n) {
		if(n == 0)
			return 0;
		else if(n == 1)
			return 1;
		else
			return function(n - 1) + 2 * function(n - 2);
	}
	
	public static int max(int a, int b) {
		if (a > b) 
			return a ;
		else if (b > a)
			return b ;
		return a ;
	}
	
	public static String hexa(int a) {
		String[] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"} ;
		String total = "";
		if (a > 16) {
			hexa(a/16) ;
			total += hex[a/16] ;
		}
		total += hex[a%16] ;
		return total ;
	}
	
	
}