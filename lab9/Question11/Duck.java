public class Duck extends Animal {
	
	private String Name ;
	
	public Duck(String Name) {
		this.Name = Name ;
	}
	
	public String getName() {
		return this.Name ;
	}
	
	public String speak() {
		return "Quack" ;
	}
}