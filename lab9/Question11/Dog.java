public class Dog extends Animal {
	
	private String Name ;
	
	public Dog(String Name) {
		this.Name = Name ;
	}
	
	public String getName() {
		return this.Name ;
	}
	
	public String speak() {
		return "Woof" ;
	}
}