public class Cat extends Animal {
	
	private String Name ;
	
	public Cat(String Name) {
		this.Name = Name ;
	}
	
	public String getName() {
		return this.Name ;
	}
	
	public String speak() {
		return "Meow" ;
	}
}