package Mustafa.main ;
import Mustafa.shapes.*;
import java.util.ArrayList ;

public class Main {
	
	public static void main(String[] args) {
		Circle circle1 = new Circle(5) ;
		Circle circle2 = new Circle(3) ;
		Circle circle3 = new Circle(10) ;
		Rectangle rectangle1 = new Rectangle(10, 5) ;
		
		System.out.println("Area of circle " + circle1.area()) ;
		System.out.println("Area of circle " + circle2.area()) ;
		System.out.println("Area of circle " + circle3.area()) ;
		System.out.println("Area of rectangle " + rectangle1.area()) ;
		
		ArrayList<Circle> circles = new ArrayList() ;
		circles.add(new Circle(3));
		circles.add(new Circle(6));
		circles.add(new Circle(7));
		
		Drawing draw = new Drawing() ;
		
		for(int i = 0; i < circles.size(); i++ ) {
			Circle a = circles.get(i) ;
			draw.addCircle(a) ;
			
		}
		
		draw.printAreas();
		
	}
	
	
}