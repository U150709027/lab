package Mustafa.shapes ;

public class Rectangle{
	int SideA ;
	int SideB ;
	public Rectangle(int a, int b) {
		SideA = a ;
		SideB = b ;
	}
	
	public int area(){
		return SideA * SideB ;
	}
	
	public int perimeter() {
		return (SideA + SideB) * 2 ;
	}
	
	
}